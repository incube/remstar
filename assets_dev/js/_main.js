$(document).ready(function() {

//init wow
	wow = new WOW({
		boxClass:     'wow',      // default
		animateClass: 'animated', // default
		offset:       0,          // default
		mobile:       true,       // default
		live:         true        // default
	})
	wow.init();

//toggle menu
	var isIE = document.all && !window.atob;
	var nav = $('nav');
	var menu = $('.toggleMenu');

	nav.addClass('slideOutLeft');
	if(isIE){
		nav.css('left', '-100%');
	}

	var toggleMenu = function(){
		if(menu.hasClass('active')){
			nav.removeClass('slideInLeft').addClass('slideOutLeft');
			menu.removeClass('active');
			if(isIE){
				nav.css('left', '-100%');
			}
		}else{
			nav.addClass('slideInLeft').removeClass('slideOutLeft');
			menu.addClass('active');
			if(isIE){
				nav.css('left', '0');
			}
		}
	}



	$('.toggleMenu').click(function(event){
		event.preventDefault();
		toggleMenu();
	})

	nav.find('a').click(function(){
		toggleMenu();
	})

//parallax
	$('body').parallax("50%", 0.7);
	if (Modernizr.mq('only screen and (min-width: 767px)')) {
		$('.parallax').parallax("50%", 0.2);
		$('.parallaxMovie, .parallaxDiffusion').parallax("50%", 0.1);
	}
	$('.parallaxContact').parallax("50%", 0.3);
	if (Modernizr.mq('only screen and (min-width: 767px)')) {
		$('.parallax').css('height', $( window ).width()*0.38);
		var the_timer;

		window.addEventListener('resize', function(){
			clearTimeout(the_timer);
			the_timer = setTimeout(function(){
				$('.parallax').css('height', $( window ).width()*0.38);
			}, 75);
		});
	}

//GOUP
	$('.goUp').click(function(event){
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $('.parallax').offset().top
		}, 1000);
	});

//carousel
	$("#carousel-example-generic").on('slide.bs.carousel', function (event) {
		var active = $(event.target).find('.carousel-inner > .item.active');
		//if needed
		// var from = active.index();
		// var next = $(event.relatedTarget);
		// var to = next.index();
		// var direction = event.direction;
		$("#carousel-example-generic").find('.carousel-inner span').addClass('animated fadeInLeft');
		$(active).find('span').removeClass('animated fadeInLeft');
	});

// validator
	$("#submit").click(function() {
		var NOM = $("#NOM").val();
		var COURRIEL = $("#COURRIEL").val();
		var MESSAGE = $("#MESSAGE").val();
		// To empty previous error/success message.
		$("#returnmessage").empty();
		$(".form-group").removeClass('has-error');

		$("#NOM, #COURRIEL, #MESSAGE").each(function(){
			if($(this).val()==""){
				$("#returnmessage").append('<p>Please fill the required field : '+$(this).attr('id')+'</p>');
				$(this).parent().addClass('has-error');
			}

		})

	});


});