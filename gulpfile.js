/********************************/
/* Version		: 1.4			*/
/* lastModif	: René Domingue */
/* date			: 2015/06/15	*/
/********************************/

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    minifyCss = require('gulp-minify-css'),
    notify = require("gulp-notify"),
    bower = require('gulp-bower'),
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect'),
    beep = require('beepbeep'),
    plumber = require('gulp-plumber');

var onError = function (err) { beep([0, 0, 0]); gutil.log(gutil.colors.green(err)); };

var config = {
  bowerDir: './assets_dev/vendor'
}
var paths = {
	src: 'assets_dev',
	dest: 'assets_dist',
	jssrc: 'assets_dev/js/**/*.js',
	csss: 'assets_dev/css/**/*.scss',
	imgSrc: 'assets_dev/img/**/*',
	imgDest: 'assets_dist/img',
	htmlSources : '**/*.html'
};

//Tâche pour le bower
gulp.task('bower', function() {
    return bower()
      .pipe(gulp.dest(config.bowerDir))
});

//Tâche qui copie les fonts suite à la fonction bower et les mets dans le DIST
gulp.task('copy', ['bower'], function() {
	return gulp.src(['./assets_dev/vendor/fontawesome/fonts/**/*'])
      .pipe(gulp.dest('./assets_dist/fonts'));
});

gulp.task('copyfonts', function() {
  return gulp.src(['./assets_dev/fonts/**/*'])
      .pipe(newer('./assets_dev/fonts/**/*'))
      .pipe(gulp.dest('./assets_dist/fonts'));
});


//Tâche concat et uglify le JS
gulp.task('concatTaskBuild', function() {
  return gulp.src(paths.jssrc)
  	.pipe(plumber({ errorHandler: onError }))
    .pipe(uglify())
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./assets_dist/js'));
});

//Tâche concat le JS
gulp.task('concatTask', function() {
  return gulp.src(paths.jssrc)
  	.pipe(newer(paths.jssrc))
    .pipe(sourcemaps.init())
    .pipe(plumber({ errorHandler: onError }))
    .pipe(concat('main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets_dist/js'))
    .pipe(livereload());
});


//Tâche qui concat les Vendors
//TODO rendre les src du vendor dynamique
gulp.task('concatTaskVendor', function() {
  return gulp.src(['./assets_dev/vendor/jquery/dist/jquery.min.js', './assets_dev/vendor/bootstrap-sass-official/assets/javascripts/bootstrap.min.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('vendor.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets_dist/js'));
});


//Tâche qui compresse les images
gulp.task('img', function() {
	return gulp.src(paths.imgSrc)
		.pipe(newer(paths.imgDest))
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
        .pipe(gulp.dest(paths.imgDest))
        .pipe(livereload());
});

//Tâche qui SASS les CSS
gulp.task('sassTask', function () {
	gulp.src(paths.csss)
		.pipe(sourcemaps.init())
		.pipe(newer(paths.csss))
		.pipe(plumber({ errorHandler: onError }))
		.pipe(sass())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./assets_dist/css'))
		.pipe(livereload());
});

//Tâche qui SASS et minify les CSS
gulp.task('sassTaskBuild', function () {
	gulp.src(paths.csss)
		.pipe(plumber({ errorHandler: onError }))
		.pipe(sass())
		.pipe(minifyCss())
		.pipe(gulp.dest('./assets_dist/css'))
});

//Tâche qui créer un serveur livereload
gulp.task('connect', function() {
  connect.server({
    livereload: true
  })
});

//Tâche qui livereload les HTML
gulp.task('html', function() {
  gulp.src(paths.htmlSources)
  .pipe(connect.reload())
});

// Tâche qui watch le js et le css
gulp.task('watchTask', function() {
	livereload.listen();
	gulp.watch(paths.jssrc, ['concatTask']);
	gulp.watch(paths.csss, ['sassTask']);
});

// Tâche DEFAULT
gulp.task('default', ['html','connect', 'watchTask','concatTaskVendor', 'sassTask', 'concatTask', 'copyfonts']);
// Tâche START, première tâche à effectuer
gulp.task('start', ['bower']);
// Tâche BUILD
gulp.task('build', ['copy', 'sassTaskBuild','concatTaskVendor', 'concatTaskBuild', 'img']);